package com.example.upravuvanjesoiktproekti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UpravuvanjeSoIktProektiApplication {

	public static void main(String[] args) {
		SpringApplication.run(UpravuvanjeSoIktProektiApplication.class, args);
	}
}
